
fft = {}
simpleFft = {}
log = ""

counter = 0
volume = 0

nLines = 33 -1
fftCircle = 0


----------------------------------------------------
function setup()

end

----------------------------------------------------
function update()

end

----------------------------------------------------
function draw()

	local gutter = 500
	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	
	of.setColor(255,0,0)
	of.drawTriangle(3648*.5*eq3,0, (3648-gutter) * eq3, 0, 0, (1080)*eq3)

	of.setColor(255)
	of.drawTriangle((3648) - ((3648-gutter)*(1.0 - eq3)),0, 3648, 1080, 0, (1080) - ((1080-gutter) * (1.0-eq3)))

	
end


----------------------------------------------------
function exit()
	--print("script finished")
end

----------------------------------------------------
function circle(radius, x, y)
	black = of.Color()
    black:set(0.0, 0.0, 0.0)

    white = of.Color()
    white:set(255.0, 255.0, 255.0)

	path = of.Path()
	path:moveTo(x, y)
	path:setFillColor(black)
	path:arc(x, y, radius, radius, 90, 270)
	path:close()
	path:draw()

	path = of.Path()
	path:moveTo(x, y)
	path:setFillColor(white)
	path:arc(x, y, radius, radius, -90, 90)
	path:close()
	path:draw()
end



-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

