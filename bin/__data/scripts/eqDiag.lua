
fft = {}
simpleFft = {}
log = ""

counter = 0
volume = 0

nLines = 33 -1
fftCircle = 0


----------------------------------------------------
function setup()

end

----------------------------------------------------
function update()

end

----------------------------------------------------
function draw()

	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	
	of.setColor(255,0,0)

	log = "coucou"
	of.pushMatrix()
	of.translate(-4000, 0)

	for i=1,1024 do

		of.setColor(200 + fft[i]*55)

		local rectWidth =  25 + fft[i] * 150
		local gutter =  70 + fft[i] * 15

		local middlexPoint =  (rectWidth * .5)
		of.pushMatrix()
			of.translate((i-1)*(rectWidth+gutter), 0)
			of.translate(middlexPoint, 1080/2)
			of.rotate(fft[i]*180)
			of.scale(1 + fft[i] * smoothVol, 1+ fft[i]* smoothVol)
			of.translate(-middlexPoint, -1080/2)

			of.drawRectangle(0, 0, rectWidth, 1080)
		of.popMatrix()

		--of.popMatrix()
	end

	of.popMatrix()


	
end


----------------------------------------------------
function exit()
	--print("script finished")
end

----------------------------------------------------
function circle(radius, x, y)
	black = of.Color()
    black:set(0.0, 0.0, 0.0)

    white = of.Color()
    white:set(255.0, 255.0, 255.0)

	path = of.Path()
	path:moveTo(x, y)
	path:setFillColor(black)
	path:arc(x, y, radius, radius, 90, 270)
	path:close()
	path:draw()

	path = of.Path()
	path:moveTo(x, y)
	path:setFillColor(white)
	path:arc(x, y, radius, radius, -90, 90)
	path:close()
	path:draw()
end



-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

