
fft = {}
simpleFft = {}
log = ""

counter = 0
volume = 0

nLines = 33 -1
fftCircle = 0
img = of.Image()
img:load("images/ittakes/ittakescourage.png")

----------------------------------------------------
function setup()

end

----------------------------------------------------
function update()

end

----------------------------------------------------
function draw()

	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	
	-- Global volume
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8

	gridX = 50
	rectHeight = 550
	startX = 174
	startY = 215

	endX = startX + (nLines*2)*gridX



	vLines = math.floor(nLines*volume)

	if vLines > 2 then
		-- Left Lines
		for i=0,vLines do
			of.setColor(255, 255, 255)
			of.drawRectangle(startX + i*gridX *2, startY, gridX , rectHeight)
		end

		-- Right Lines
		for i=0,vLines do
			of.setColor(255, 255, 255)
			of.drawRectangle(endX - (i*gridX*2), startY, gridX , rectHeight)
		end
	end

	if vLines < 2 then
		img:draw(0,0)
	end

	-- EQ
	maxCircle = math.floor( (rectHeight-gridX*2) / (gridX *2))

	for i=1,32 do
		index = i*(512/32)
		fftCircle = math.floor(maxCircle*fft[index]) -1

		for j=0,fftCircle do
			circle(gridX , startX+gridX*i +gridX*(i-1), startY+rectHeight-gridX*2 -gridX*2*j )
		end
	end
	
end


----------------------------------------------------
function exit()
	--print("script finished")
end

----------------------------------------------------
function circle(radius, x, y)
	black = of.Color()
    black:set(0.0, 0.0, 0.0)

    white = of.Color()
    white:set(255.0, 255.0, 255.0)

	path = of.Path()
	path:moveTo(x, y)
	path:setFillColor(black)
	path:arc(x, y, radius, radius, 90, 270)
	path:close()
	path:draw()

	path = of.Path()
	path:moveTo(x, y)
	path:setFillColor(white)
	path:arc(x, y, radius, radius, -90, 90)
	path:close()
	path:draw()
end



-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

