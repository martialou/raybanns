
fft = {}
simpleFft = {}
log = ""

counter = 0
volume = 0

size = 2
sizeMax = 50
maxLine = 200
nLine = 0

p1x = 3648/2
p1y = 1080/2

p2x = 0
p2y = 0

stepX = 3648 / maxLine /4
stepY = 1080 / maxLine /4

directionX = 0
directionY = 0

----------------------------------------------------
function setup()


end

----------------------------------------------------
function update()


end

----------------------------------------------------
function draw()

	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	
	-- Global volume
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8


	nLine = math.floor(maxLine*volume)
	
	of.setLineWidth(4)
	of.setHexColor(0xFFFFFF)

	for i=1,nLine do
		--[[
		if p2x < 0 then
			directionX = 1
		elseif p2x > 3648 then
			directionX = -1
		end

		if p2y < 0 then
			directionY = 1
		elseif p2y > 1080 then
			directionY = -1
		end
		--]]
		if p2x >= 3648 then
			p2y = p2y + stepY
		elseif p2x <= 0 then
			p2y = p2y -stepY
		end

		if p2y >= 1080 then
			p2x = p2x - stepX
		elseif p2y <= 0 then
			p2x = p2x + stepX
		end
		log = "line: "..tostring(nLine).." /p2x: "..tostring(p2x).." /p2y: "..tostring(p2y)
		of.setLineWidth(4)
		of.setHexColor(0xFFFFFF)
		of.drawLine(p1x, p1y, p2x, p2y)

	end
	
end


----------------------------------------------------
function exit()
	--print("script finished")
end



-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

