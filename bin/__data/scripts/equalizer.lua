
fft = {
simpleFft = {}}
counter = 0
bSmooth = false

log = ""


----------------------------------------------------
function setup()
	fft = {}
end

----------------------------------------------------
function update()

	
	
	--fftToTable()
	counter = counter + 0.033


end

----------------------------------------------------
function draw()
	
	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	
	rectSize = 3648 / 8
	for i=1,8 do
		of.fill()
		of.setColor(255,0,0)
		of.drawRectangle((i-1)*rectSize, 1080, rectSize, -(simpleFft[i] * 1040))
		of.setColor(255,255,255)
		of.drawRectangle((i-1)*rectSize, 0, rectSize, (1.0-simpleFft[i]) * 1040)
		of.noFill()
		of.setColor(0,0,0)
		of.drawRectangle((i-1)*rectSize, 0, rectSize, (1.0-simpleFft[i]) * 1040)
	end
	
end


----------------------------------------------------
function exit()
	--print("script finished")
end

-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

