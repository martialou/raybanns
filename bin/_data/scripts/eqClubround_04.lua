
fft = {}
simpleFft = {}
log = ""

counter = 0
volume = 0

size = 2
sizeMax = 50
maxLine = 300
nLine = 0

-- multiColors
colors = {0xFFFFFF, 0xF61F00}
colorID = math.floor(math.random(1,2))
multiColor = math.random(0,1)

img1 = of.Image()
img2 = of.Image()

img1:load("images/clubround/clubround-mask-01.png")
img2:load("images/clubround/clubround-mask-02.png")

----------------------------------------------------
function setup()
	multiColor = math.random(0,1)
end

----------------------------------------------------
function update()


end

----------------------------------------------------
function draw()

	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	
	-- Global volume
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8


	nLine = math.floor(maxLine*volume)
	of.setHexColor(colors[colorID])
	of.setLineWidth(4)

	for i=1,nLine do

		if multiColor > 0.45 then
			colorID = math.floor(math.random(1,2))
			of.setHexColor(colors[colorID])
		end
		
		if math.random(0,1) > 0.5 then
			of.drawLine(0, math.random(1080), 3648, math.random(1080))
		else
			of.drawLine(math.random(3648), 0, math.random(3648), 1080)
		end

	end

	if volume > 0.60 then
		img1:draw(0,0)
	else
		img2:draw(0,0)
	end
	
end


----------------------------------------------------
function exit()
	--print("script finished")
end



-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

