
fft = {
simpleFft = {}}
counter = 0
log = ""

volume = 0
fps = 60
timer = 4
exponent = 1


----------------------------------------------------
function setup()
	fft = {}
end

----------------------------------------------------
function update()

	--fftToTable()

end

----------------------------------------------------
function draw()
	
	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }

--- Global volume
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8
	
	direction = 1
	eqCount = 1

	exponent = math.floor(volume*4) + 1

	eq = 2^exponent
	eq = eq^2

	if eq > 4 then
		eqSpace = (3648/eq)/2
		eqSize = eqSpace
	else
		eqSpace = 0
		eqSize = 3648/eq
	end

	

	for i=1,eq do
		of.fill()
		of.setColor(255,255,255)

		index = i*(512/eq)
		of.drawRectangle((i-1)*(eqSize+eqSpace), (1080/2)*(1+direction*-1), eqSize, (fft[index] * 1080)*direction)

		if eq > 4 then
			if eqCount > eq/4 -1 then
				eqCount = 1
				direction = -1*direction
			else
				eqCount = eqCount +1
			end
		else
			direction = -1*direction
		end

		log = "Eqcount : "..tostring(eqCount)
			
	end
	
end


----------------------------------------------------
function exit()
	--print("script finished")
end

-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

