
fft = {}
simpleFft = {}
log = ""

counter = 0
volume = 0

nLines = 33 -1
fftCircle = 0

--- volume Ratio

vMin = 0.2
vMax = 0.8
vAmp = vMax - vMin
vRatio = 0

nStep = 20
step = 0


----------------------------------------------------
function setup()
	fft = {}
end

----------------------------------------------------
function update()

end

----------------------------------------------------
function draw()

	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	
	-- Global volume
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8

	vRatio = 1.0
	-- map volume a .2 > 1
	--vRatio = map(volume, 0.0, 1.0, 0.2, 1.0)
	print(vRatio)

	--vRatio = vMin + volume*vMax
	--step = math.floor(nStep*vRatio)
	print(step)


	log = "vol: "..tostring(volume).." / vRatio: "..tostring(vRatio).."/ step: "..tostring(step)

	gridX = 50
	rectHeight = 550
	startX = 174
	startY = 215

	endX = startX + (nLines*2)*gridX

	vLines = math.floor(nLines*volume)

	if vLines > 2 then
		-- Left Lines
		for i=0,vLines do
			of.setColor(255, 255, 255)
			of.drawRectangle(startX + i*gridX *2, startY, gridX , rectHeight)
		end

		-- Right Lines
		for i=0,vLines do
			of.setColor(255, 255, 255)
			of.drawRectangle(endX - (i*gridX*2), startY, gridX , rectHeight)
		end
	end

	
end


----------------------------------------------------
function exit()
	--print("script finished")
end

----------------------------------------------------

function map( value,  inputMin,  inputMax,  outputMin,  outputMax) {

	
	return ((value - inputMin) / (inputMax - inputMin) * (outputMax - outputMin) + outputMin)
	

}


-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

