
fft = {}
simpleFft = {}
log = ""

counter = 0
timer = 3 --60*0.05
volume = 0

size = 2
sizeMax = 20
maxSquare = 1000
nSquare = 0

histogram = {}
histogramCounter = 0
eqSpace = 200
----------------------------------------------------
function setup()


end

----------------------------------------------------
function update()

	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }

--- Global volume 
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8

	if counter > timer then
		posInArray = histogramCounter % math.ceil(3648/eqSpace)
    	histogram[posInArray] = volume
    	histogramCounter = histogramCounter +1

    	counter = 0
    else 

    	counter = counter +1

    end

end

----------------------------------------------------
function draw()


	
		if histogramCounter < math.ceil(3648/eqSpace) then
			log = tostring(histogramCounter)

			for i=1,histogramCounter do
				of.setColor(255,0,0)
				drawActivity (i*eqSpace, histogram[i])
			end
		else
			startpos = histogramCounter
			endpos =  histogramCounter + math.ceil(3648/eqSpace)

			for i=startpos,endpos do
				posInArray =   i % math.ceil(3648/eqSpace)
				--log = tostring(math.ceil(posInArray))
				of.setColor(255,0,0)
				drawActivity ((i-startpos)*eqSpace, histogram[math.ceil(posInArray)])
			end
			
		end

end


----------------------------------------------------
function exit()
	--print("script finished")
end

function drawActivity (x, vol)
	nPattern = 10
	patternID = math.floor(vol*nPattern)

	colors = {0xFFFFFF, 0xF61F00}
	of.fill()

	if patternID == 1 then
		of.setHexColor(colors[1])
		of.setLineWidth(4)
		of.drawLine(x, 1080*0.4, x+1080*0.2, 1080*0.6)
		of.drawLine(x, 1080*0.6, x+1080*0.2, 1080*0.4)

	elseif patternID == 2 then
		of.setHexColor(colors[1])
		of.drawCircle(x, 1080*0.5, 50)

	elseif patternID == 3 then
		of.setHexColor(colors[2])
		of.drawCircle(x, 1080*0.5 - 50*2, 50)
		of.drawCircle(x, 1080*0.5 + 50*2, 50)
		of.setHexColor(colors[1])
		of.drawCircle(x, 1080*0.5, 50)

	elseif patternID == 4 then
		of.setHexColor(colors[1])
		of.drawRectangle(x, 1080*0.2, eqSpace/4, 1080*0.6)

	elseif patternID == 5 then
		of.setHexColor(colors[2])
		of.drawCircle(x, 1080*0.5, 100)

	elseif patternID == 6 then
		of.setHexColor(colors[2])

		nRect = 5
		patternHeight = 0.5
		rectHeight = 1080*patternHeight / (nRect * 2)

		for i=1,nRect do
			of.drawRectangle(x, 1080*(patternHeight/2) + (i-1)*rectHeight*2, eqSpace, rectHeight)
		end

	elseif patternID == 7 then
		of.setHexColor(colors[1])
		of.drawCircle(x, 1080*0.4, 75)
		of.drawCircle(x, 1080*0.6, 75)

	elseif patternID == 8 then
		of.setHexColor(colors[2])

		nRect = 3
		rectWidth = eqSpace/(nRect*2)

		for i=1,nRect do
			of.drawRectangle(x +i*rectWidth*2, 1080*0.1, rectWidth, 1080*0.8)
		end

	elseif patternID == 9 then
		of.fill()
		of.setHexColor(colors[1])
		of.drawCircle(x, 1080*0.5, 300)

	elseif patternID == 10 then
		of.fill()
		of.setHexColor(colors[2])
		of.drawCircle(x, 1080*0.5, 300)

	else
		log = "NO PATTERN : "..tostring(patternID)
	end
end


-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

