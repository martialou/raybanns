
fft = {}
simpleFft = {}

nTriangles = 48
bSmooth = false

counter = 0

colors = {0xFFFFFF, 0x000000, 0xF61F00}

log = ""



Mountain = class()

function Mountain:__init(x1, x2, fftIndex, hexColor, stepIndex)
	self.x1 = x1
	self.x2 = x2
	self.x3 = x1 + (x2 - x1) * .5
	self.fftIndex = fftIndex
	self.hexColor = hexColor
	self.speed = (4 - stepIndex) / .2
	self.stepIndex = stepIndex

end

function Mountain:draw(fft)
	of.setHexColor(self.hexColor)
	of.drawTriangle(self.x1, 1080, self.x2, 1080, self.x3, (1.0 - simpleFft[self.fftIndex]) * 1080 )

end

function Mountain:move()
	self.x1 = self.x1 - self.speed
	self.x2 = self.x2 - self.speed
	self.x3 = self.x1 + (self.x2 - self.x1) * .5

	if self.x2 < 0 then
		local diff = self.x2 - self.x1
		self.x1 = 3648 
		self.x2 = 3648 + diff
		self.x3 = self.x1 + (self.x2 - self.x1) * .5
	end

end

	mountains = {}
	step = nTriangles / 3
	hexIndex = 1
	stepCount = 0
	for i=1,nTriangles do
		x1 	= math.random() * (4000-200)
		x2 = x1 + math.random() * (( 4000 - x1 ))
		while x2-x1 < 200 or x2-x1 > 1000 do
			x2 = x1 + math.random() * (( 4000 - x1 ))
		end
		fftIndex = 1 + math.floor(math.random() * 7)
		
		table.insert(mountains, i, Mountain(x1, x2, fftIndex, colors[hexIndex], hexIndex))
		
		stepCount = stepCount+1
		if stepCount >  step
			then
			stepCount = 0
			hexIndex = hexIndex +1
		end

	end

----------------------------------------------------
function setup()

	

	
end

----------------------------------------------------
function update()


	counter = counter+1

	for i=1,nTriangles do
		mountains[i]:move()

	end

end

----------------------------------------------------
function draw()
	
	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }

	--log ="bitch"
	
	for i=1,nTriangles do
		mountains[i]:draw(fft)
	end

	--[[
	c = of.Color()
  	c:set(127.0, 127.0, 255.0)

  	for i=1,1024 do
  		of.setColor(255,0,0)
  		of.drawRectangle(i, 0, 1, 200*fft[i])
  	end
  	--]]
	
	

end


----------------------------------------------------
function exit()
	--print("script finished")
end

-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

