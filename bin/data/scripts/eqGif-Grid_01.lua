
fft = {}
simpleFft = {}
log = ""

counter = 0
volume = 0

gridX = 16
gridY = 16

matrix = {}
matrixSize = gridX*gridY
countSquare = 1

squareWidth = 3648/gridX
squareHeight = 1080/gridY

-- randomColor
colors = {0xFFFFFF, 0xF61F00, 0x000000}
colorID = math.floor(math.random(1,3))

----------------------------------------------------
function setup()
	colorID = math.floor(math.random(1,3))

	for i=1,matrixSize do
		matrix[i] = 0
	end

end

----------------------------------------------------
function update()


end

----------------------------------------------------
function draw()
	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	-- Global volume
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8

	for i=1,matrixSize do
		matrix[i] = 0
	end


	nSquare = math.floor(matrixSize*volume)
	countSquare = 1

	of.setHexColor(colors[colorID])

	for i=1,nSquare do
		sqID = math.floor(math.random(matrixSize))

		if countSquare < matrixSize then
			while matrix[sqID] == 1 do
				sqID = math.floor(math.random(matrixSize))
			end
		end

		lineID = math.ceil(sqID/gridX)
		cellID = math.abs(sqID - lineID*gridX)
		matrix[sqID] = 1
		countSquare = countSquare +1

		of.drawRectangle(cellID*squareWidth, lineID*squareHeight, squareWidth, squareHeight)

		
	end
	
end


----------------------------------------------------
function exit()
	--print("script finished")
end



-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

