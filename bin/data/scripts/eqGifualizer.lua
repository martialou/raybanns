
fft = {
simpleFft = {}}
counter = 0
bSmooth = false

log = ""


-- randomColor
colors = {0xFFFFFF, 0xF61F00, 0x000000}
colorID = math.floor(math.random(1,3))

----------------------------------------------------
function setup()
	colorID = math.floor(math.random(1,3))
	fft = {}
end

----------------------------------------------------
function update()

	
	
	--fftToTable()
	counter = counter + 0.033


end

----------------------------------------------------
function draw()
	
	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }

	rectSize = 3648 / 8

	of.fill()
	of.setHexColor(colors[colorID])

	for i=1,8 do
		of.drawRectangle((i-1)*rectSize, 1080, rectSize, -(simpleFft[i] * 1040))
	end
	
end


----------------------------------------------------
function exit()
	--print("script finished")
end

-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

