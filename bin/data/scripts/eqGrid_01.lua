
fft = {}
simpleFft = {}
log = ""

counter = 0
volume = 0

gridX = 16
gridY = 16

matrix = {}
matrixSize = gridX*gridY
countSquare = 1

squareWidth = 3648/gridX
squareHeight = 1080/gridY

-- multicolor
colors = { 0xFFFFFF, 0xF61F00}
colorID = math.floor(math.random(1,2))
multiColor = math.random(0,1)
nSquare = 0
counter = 0
----------------------------------------------------
function setup()

	counter = 0
	multiColor = math.random(0,1)

	for i=1,matrixSize do
		matrix[i] = 0
	end

end

----------------------------------------------------
function update()

	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }
	-- Global volume
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8

	for i=1,matrixSize do
		matrix[i] = 0
	end
	if counter % 3 == 0 then
		nSquare = math.floor(matrixSize*volume)
	end
	counter++
end

----------------------------------------------------
function draw()
	

	log = "multi: "..tostring(multiColor).." / id: "..tostring(colorID)

	countSquare = 1

	of.setHexColor(colors[colorID])

	for i=1,nSquare do
		sqID = math.floor(math.random(matrixSize))

		if countSquare < matrixSize then
			while matrix[sqID] == 1 do
				sqID = math.floor(math.random(matrixSize))
			end
		end

		lineID = math.ceil(sqID/gridX)
		cellID = math.abs(sqID - lineID*gridX)
		matrix[sqID] = 1
		countSquare = countSquare +1

		if multiColor > 0.65 then
			colorID = math.floor(math.random(1,2))
			of.setHexColor(colors[colorID])
			--log = "MULTICOLOR !!!!!!"
		end

		of.drawRectangle(cellID*squareWidth, lineID*squareHeight, squareWidth, squareHeight)

		
	end
	
end


----------------------------------------------------
function exit()
	--print("script finished")
end



-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

