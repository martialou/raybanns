
fft = {}
simpleFft = {}
log = ""

counter = 0
timer = 1 --60*0.05
volume = 0

changeColor = false

histogram = {}
histogramCounter = 0
rectWidth = 20

----------------------------------------------------
function setup()


end

----------------------------------------------------
function update()

	simpleFft = { eq0, eq1, eq2, eq3, eq4, eq5, eq6, eq7 }

--- Global volume 
	for i=1,8 do
		volume = volume + simpleFft[i]
	end

	volume = volume / 8

	--if counter > timer then
		posInArray = histogramCounter % math.ceil(3648/rectWidth)
    	histogram[posInArray] = volume
    	histogramCounter = histogramCounter +1

    --[[	counter = 0
    else 

    	counter = counter +1

    end
    ]]--


end

----------------------------------------------------
function draw()

	color = 1
	if histogramCounter < math.ceil(3648/rectWidth) then
		log = tostring(histogramCounter)

		for i=1,histogramCounter do
  			drawActivity(i*rectWidth, histogram[i])
		end
	else
		startpos = histogramCounter
		endpos =  histogramCounter + math.ceil(3648/rectWidth)

		for i=startpos,endpos do
			posInArray =   i % math.ceil(3648/rectWidth)
			of.setColor(255,0,0)
			log = tostring(math.ceil(posInArray))

  			drawActivity((i-startpos)*rectWidth, histogram[math.ceil(posInArray)])
		end
		

	end

    --log = tostring(3)
end


----------------------------------------------------
function exit()
	--print("script finished")
end

----------------------------------------------------
function drawActivity (x, vol)
	
	maxRect = 70;
	nRect = math.floor(maxRect*vol)
	rectHeight = 1080/(nRect*2);

	color = 1
	colors = {0xFFFFFF, 0xF61F00}
	of.fill()
	of.setHexColor(colors[color])

	for r=0,nRect do
		if changeColor then
			of.setHexColor(colors[color])
		end
		of.drawRectangle(x, r*rectHeight*2, rectWidth, rectHeight)
		
		if color == 1 then
			color = 2
		else
			color = 1
		end
	end

end



-- input callbacks

----------------------------------------------------
function keyPressed(key)
	-- print out key as ascii val & char (keep within ascii 0-127 range)
	print("script keyPressed: "..tostring(key)
		.." \'"..string.char(math.max(math.min(key, 127), 0)).."\'")
	if key == string.byte("s") then
		bSmooth = not bSmooth
	end
end

