//
//  CanvasDrawable.hpp
//  RayBanNS
//
//  Created by Martial on 19/04/16.
//
//

#ifndef CanvasDrawable_hpp
#define CanvasDrawable_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxAnimatableFloat.h"


class CanvasDrawable {
    
public:
    
    CanvasDrawable() {
        
        bActive = false;
        opacity.reset(0.0f);
        opacity.setCurve(EASE_IN);
        opacity.setRepeatType(PLAY_ONCE);
        bHasMask = false;
    
    };
    
    virtual void    setup() {};
    virtual void    update(float dt){
        
        opacity.update(dt);
        
    };
    
    virtual void    init(){};
    virtual void    exit(){};
    
    virtual void    draw(){};
    
    virtual void    isReady() {
        return true;
    }
    
    void            onVolumeHandler(float pct);
    void            onLowFreqHandler(float pct);
    void            onMidFreqHandler(float pct);
    void            onHighFreqHandler(float pct);
    
    string                      name;
    string                      type;
    int                         zIndex ;
    bool                        bActive;
    
    ofxAnimatableFloat           opacity;
    
    bool                        bHasMask;
    
    
    
};

#endif /* CanvasDrawable_hpp */
