//
//  CanvasManager.cpp
//  RayBanNS
//
//  Created by Martial on 19/04/16.
//
//

#include "CanvasManager.hpp"
#include "ImgDrawable.hpp"
#include "VideoDrawable.hpp"
#include "LuaDrawable.hpp"
#include "GifDrawable.hpp"

#include "ofApp.h"

CanvasManager::CanvasManager() {
    
    //currentLayers = NULL
    log = "ahou";
    groupIndex = 0;
    totalClaims = 0;
    totalEqualizer = 0;
    equalizerIndex = 0;
    equalizerLucks = 0.45;
    bSwitchBrand = false;
}

void CanvasManager::setup(FBOCanvas * fboCanvas) {
    
    this->fboCanvas = fboCanvas;
    transitionManager.setup(this, fboCanvas);
    // init the lua state
   // lua.init(true);
   // lua.addListener(this);
    //lua.doScript("variables.lua");
    
    transitionTimer.setup(this);
    
    mask.setup();
    

}

void CanvasManager::setFromXml() {
    
    // first load videos
    ofxXmlSettings xml;
    
    if(xml.load("new_video_settings.xml")) {
        
        ofLog(OF_LOG_NOTICE, "parse xml ");

        
        xml.pushTag("root");
        
        xml.pushTag("settings");
        
        soundDeviceId               = xml.getValue("sound", 0 , 0);
        //transitionTimerDelay        = xml.getValue("transitiontimer", 5000 , 0);
        //transitionTimer.duration    = transitionTimerDelay;
        xml.popTag();
        
        ofDirectory gifDir("gifs");
        //only show png files
        //dir.allowExt("lua");
        //populate the directory object
        gifDir.listDir();
        
        //go through and print out all the paths
        for(int i = 0; i < gifDir.size(); i++){
            
            ofLogNotice(gifDir.getPath(i));
            
            GifGroupDrawable * gifGroup = new GifGroupDrawable();
            
            
            gifGroup->name = gifDir.getFile(i).getFileName();
            
            ofDirectory gifFolder(gifDir.getPath(i));
            gifFolder.listDir();
            gifFolder.allowExt("gif");
            for(int j = 0; j < gifFolder.size(); j++){
                // ofLogNotice(gifFolder.getPath(j));
                gifGroup->load(gifFolder.getPath(j));
                
                
            }
            gifGroup->startLoading();
            layers.push_back(gifGroup);
            
        }
        
        xml.pushTag("videos");
        
        
        int numVideos = xml.getNumTags("video");
        for(int i=0; i<numVideos; i++) {
            
            //xml.pushTag("video", i);
            string path = xml.getValue("video", "" , i);
            string name = xml.getValue("video", "" , i);
            
            // check extension
            
            size_t position = path.find(".");
            string extension = (string::npos == position)? path : path.substr(position, path.size());
            
            if(extension == ".mov") {
            
                VideoDrawable * video = new VideoDrawable();
                //shared_ptr<VideoDrawable> video (new VideoDrawable());
                
                int   soundReactive  = xml.getAttribute("video", "soundReactive", 0, i);
                int   reverse  = xml.getAttribute("video", "reverse", 0, i);
                

            
                
                video->name = name;
                video->bSoundReactive = soundReactive;
                video->bReverse = reverse;
                video->loadVideo(path);
                layers.push_back(video);
                
               // ofLog(OF_LOG_NOTICE, "pus video ");

                
            } else if (extension == ".gif") {
                
                GifDrawable * gif = new GifDrawable();
                //shared_ptr<GifDrawable> gif (new GifDrawable());

                gif->name = name;
                gif->load(path);
                layers.push_back(gif);
                
            }

           // xml.popTag();
            
        }
        xml.popTag();
        
        //ofLog(OF_LOG_NOTICE, "gogo ");

        
        // then load scenariis
        xml.pushTag("combos");
        
        int numGroups = xml.getNumTags("group");
        ofLog(OF_LOG_NOTICE, "n groups %d", numGroups);

        for(int u=0; u<numGroups; u++) {

           
            
            ScenariiGroupData scenariiGroup;
            scenariiGroup.type = xml.getAttribute("group", "type" , "null", u);
            scenariiGroup.timeMin = xml.getAttribute("group", "timemin" , 5000, u);
            scenariiGroup.timeMax = xml.getAttribute("group", "timeMax" , 10000, u);
            ofLog(OF_LOG_NOTICE,"grup min max %d - %d",scenariiGroup.timeMin,  scenariiGroup.timeMax);

            
            
            if(scenariiGroup.type == "claim")
                totalClaims ++;
            
            
            ofLog(OF_LOG_NOTICE, "group type " + ofToString(scenariiGroup.type));

            xml.pushTag("group", u);
            
        
            int numCombos = xml.getNumTags("combo");
            
            
            if(scenariiGroup.type == "equalizer")
                totalEqualizer = numCombos;
            
            for(int i=0; i<numCombos; i++) {
            

                xml.pushTag("combo", i);
            
                ScenariiData scenariiData;
                scenariiData.id = scenariis.size();

                int numLayers = xml.getNumTags("file");
            
                for(int j=0; j<numLayers; j++) {
                
                    Scenarii sc;

                    string name     = xml.getValue("file", "" , j);
               // xml.pushTag("file",j);
                    int   useMask  = xml.getAttribute("file", "useMask", 0, j);
                //xml.popTag();
                
                    sc.name = name;
                    sc.bUseMask = useMask;
                
                    scenariiData.layers.push_back(sc);
                }
                scenariis.push_back(scenariiData);
                scenariiGroup.scenariis.push_back(scenariiData);
                xml.popTag();
            
            }
            xml.popTag();
            ofLog(OF_LOG_NOTICE, "group with %d ", scenariiGroup.scenariis.size());

            scenariiGroups.push_back(scenariiGroup);
        }
        xml.popTag();
        
        /*
        // then load transitions
        xml.pushTag("transitions");
        int numTransitions = xml.getNumTags("transition");
        for(int i=0; i<numTransitions; i++) {
            
            TransitionData transition;
            transition.durationIn = xml.getAttribute("transition", "durationIn", 1000.0f, i);
            transition.durationOut = xml.getAttribute("transition", "durationOut", 1000.0f, i);
            transition.easeIn = xml.getAttribute("transition", "easeIn", "EASE_IN", i);
            transition.easeOut = xml.getAttribute("transition", "easeOut", "EASE_OUT", i);

            xml.pushTag("transition", i);

            transition.fade     = (xml.getValue("fade", "true" , 0) == "true" ) ? true : false;
            transition.rgbShift = (xml.getValue("rgbShift", "true" , 0) == "true" ) ? true : false;
            transition.blur = (xml.getValue("blur", "true" , 0) == "true" ) ? true : false;

            
            transitions.push_back(transition);
            xml.popTag();
            
        }
        xml.popTag();
        */
        
        xml.popTag();
        
    } else {
        
        ofLog(OF_LOG_NOTICE, "error xml ");

    }
    
    
    ofDirectory dir("scripts");
    //only show png files
    dir.allowExt("lua");
    //populate the directory object
    dir.listDir();
    
    //go through and print out all the paths
    for(int i = 0; i < dir.size(); i++){
        
        LuaDrawable * luaScript = new LuaDrawable();
        luaScript->setMom(this);
        //LuaDrawable> luaScript (new LuaDrawable());

        luaScript->loadScript(dir.getPath(i));
        //luaScript->setLua(&lua);
        luaScript->name = dir.getFile(i).getFileName();
        
        layers.push_back(luaScript);
        ofLogNotice(dir.getPath(i));
        
    }
    

    
   
    
    for( auto layer : layers ) {
        layer->setup();
    }
    
    
}

CanvasDrawable  * CanvasManager::getLayerByName(string name) {
    
    for( auto layer : layers ) {
        
        if(ofToUpper(layer->name) == ofToUpper(name))
            return layer;
    }
    
    return NULL;
    
}

void CanvasManager::loadLayer(string name) {
    
    /*
    shared_ptr<CanvasDrawable>  * layer = getLayerByName(name);
    inLayers.clear();
    inLayers.push_back(*layer);
    transitionManager.startTransition(&transitions[0], &currentLayers, &inLayers);
    currentLayers.clear();
    currentLayers = inLayers;
     
     */
    
    for( auto layer : layers ) {
        layer->bActive = false;
        layer->exit();

    }
    
    CanvasDrawable  * layer = getLayerByName(name);
    layer->bActive = true;
    layer->opacity.reset(255);
    layer->init();
    layer = NULL;
    //getLayerByName(name)->bActive = true;
    
    
}


void CanvasManager::loadScenarii(int index) {
    
    ofLog(OF_LOG_NOTICE, "load scenarii %d ",index);
    
    for( auto layer : layers ) {
        layer->bActive = false;
        layer->opacity.reset(0);
        layer->zIndex = 999;
        layer->exit();


    }
    
    ScenariiData * scenarii = &scenariis[index];
    
    int i =0;
    int zIndex = 0;
    for( auto &sc : scenarii->layers ) {
        ofLog(OF_LOG_NOTICE, "layer " + ofToString(sc.name));

        CanvasDrawable  * layer = getLayerByName(sc.name);
        if(layer) {
            //inLayers.push_back(layer);
            
            layer->zIndex = zIndex;
            layer->bHasMask = sc.bUseMask;
            layer->opacity.reset(255);
            layer->bActive = true;
            layer->init();

            zIndex++;
            

            
            layer = NULL;
        }
    }
    
    scenarii = NULL;
    
    std::sort(layers.begin(), layers.end(), [](const CanvasDrawable *a, const CanvasDrawable *b) {return a->zIndex < b->zIndex;});

    
    
    /*
    ScenariiData * scenarii = &scenariis[index];
    
    // we need to attrib layers & zindex
    int zIndex = 0;
    for( auto &layer : layers ) {
        if(layer->bActive) {
            //layer->zIndex = zIndex;
            //zIndex++;
        } else {
            layer->zIndex = 999;
        }
    }
    
    
    
    // maybe clear here ?
    
    ofAddListener(transitionManager.onTransationInEvent, this, &CanvasManager::onTransitionInHandler);
    
    for( auto &layer : inLayers ) {
        transitionManager.startTransitionOut(&transitions[0], layer);
        layer = NULL;
    }
    
    inLayers.clear();

    
    int i =0;
     zIndex = 0;
    for( auto &sc : scenarii->layers ) {
        
        CanvasDrawable  * layer = getLayerByName(sc.name);
        if(layer) {
            inLayers.push_back(layer);
            
            layer->zIndex = zIndex;
            layer->bHasMask = sc.bUseMask;
            zIndex++;
            
            transitionManager.startTransitionIn(&transitions[0], layer);


        }
    }
    
    transitionManager.setTimerIn();
    
    //sort(layers.begin(), layers.end(), sortByZIndex());

    std::sort(layers.begin(), layers.end(), [](const CanvasDrawable *a, const CanvasDrawable *b) {return a->zIndex < b->zIndex;});
     
     */

    
    mask.toggleState();
    hasLuaScript();
}

void CanvasManager::loadRandomScenarii() {
    
    // so
    // first of all if rdm, go for type equalizer
    
       bSwitchBrand = !bSwitchBrand;
    if(bSwitchBrand) {

        loadType = "rayban";
        loadRandomScenariiIntroGroup();
        
    } else {
        
        float rdm = ofRandom(1.0);
        if(rdm < equalizerLucks) {
            // get random group in equalizer
            
            loadType = "equalizer";
            loadRandomScenariiIntroGroup();
            
            
        } else {
            
            groupIndex ++;
            if(groupIndex >= totalClaims) {
                groupIndex = 0;
            }

        
            loadType = "claim";
            loadRandomScenariiIntroGroup();
            
        }

        
    }
        
    
    
    
    if(loadType == "rayban") {
        for(int i=0; i<scenariiGroups.size(); i++) {
            if(scenariiGroups[i].type == "rayban") {
                transitionTimer.resetTimers(ofRandom(scenariiGroups[i].timeMin*1000, scenariiGroups[i].timeMax*1000), 1);
            }
        }
    }
    
    if(loadType == "equalizer") {
        for(auto &scgroup : scenariiGroups) {
            if(scgroup.type == "equalizer") {

                transitionTimer.resetTimers(ofRandom(scgroup.timeMin*1000, scgroup.timeMax*1000), 1);
                
            }
        }
    }
    
    
    if(loadType == "claim") {
        
        int claimCount = 0;
        for(int i=0; i<scenariiGroups.size(); i++) {
            if(scenariiGroups[i].type == "claim") {
                if(claimCount == groupIndex ) {
                    transitionTimer.resetTimers(ofRandom(scenariiGroups[i].timeMin*1000, scenariiGroups[i].timeMax*1000), 6);
                 }
                claimCount++;
            }
        }
    }

   // int rdm = (floor)(ofRandom(scenariis.size() -1));
    //loadScenarii(rdm);
    
}

void CanvasManager::loadRandomScenariiIntroGroup() {
    
    if(loadType == "rayban") {
        for(int i=0; i<scenariiGroups.size(); i++) {
            if(scenariiGroups[i].type == "rayban") {
                

                


                int rdm = floor(ofRandom(scenariiGroups[i].scenariis.size()));
                loadScenarii(scenariiGroups[i].scenariis[rdm].id);
                ofLog(OF_LOG_NOTICE,"Load into rayban");
                return;
            }
        }
    }
    
    if(loadType == "equalizer") {
        for(auto &scgroup : scenariiGroups) {
            if(scgroup.type == "equalizer") {
                
               
                
                
                //int rdm = floor(ofRandom(scgroup.scenariis.size()));
                loadScenarii(scgroup.scenariis[equalizerIndex].id);
                loadType = "equalizer";
                
                equalizerIndex++;
                if(equalizerIndex >= totalEqualizer )
                    equalizerIndex = 0;
                
                ofLog(OF_LOG_NOTICE,"Load into equalizer");
                return;
            }
        }
    }

    
    if(loadType == "claim") {

        int claimCount = 0;
        for(int i=0; i<scenariiGroups.size(); i++) {
            if(scenariiGroups[i].type == "claim") {
                if(claimCount == groupIndex ) {
    
                    int rdm = floor(ofRandom(scenariiGroups[i].scenariis.size()));
                    loadScenarii(scenariiGroups[i].scenariis[rdm].id);
                    ofLog(OF_LOG_NOTICE,"Load into claim %d - %d",groupIndex,  rdm);

                    return;
                }
                claimCount++;
            }
        }
    }
}


void CanvasManager::onScenariiLoadedHandler() {
    
    
}

void CanvasManager::onTransitionInHandler(ofEventArgs & e) {
    
    // transition back
    
    
    for( auto layer : layers ) {
        layer->bActive = false;
    }
    
    for(auto layer : inLayers) {
        layer->bActive = true;
        transitionManager.startTransitionInBack(&transitions[0], layer);

    }
    
}

vector<string> CanvasManager::getLayersNames(string type) {
    
    vector<string> result;
    for( auto layer : layers ) {
        if(layer->type == type)
        result.push_back(layer->name);
    }
    
    return result;
}

vector<string> CanvasManager::getScenariisNames() {
    
    vector<string> result;
    int i=0;
    for( auto scenarii : scenariis ) {
        result.push_back(ofToString(i++));
    }
    
    return result;
    
}


void CanvasManager::update() {
    
    transitionTimer.update();

    mask.update();
    
    float dt = 1.0f / 60.0f;
    transitionManager.update(dt);
    
    for( auto layer : layers ) {
        if(layer->bActive)
            layer->update(dt);
        
    }
    
    //lua.setString("ahou", "test");
}

void CanvasManager::draw() {
    
    ofSetColor(255,255,255,255);
    for( auto layer : layers ) {
        if(layer->bActive) {

            if(layer->bHasMask) {
                mask.begin();
                layer->draw();
                mask.end();
                mask.draw();
            } else {
                layer->draw();

            }
            
            
        }
    }
}

void CanvasManager::drawLuaLog() {
    
    ofSetColor(255,255,255);

    ofDrawBitmapString(log, fboCanvas->x, fboCanvas->y + fboCanvas->height+  40 );

    for(int i=0; i<ofClamp(luaLogs.size(), 0, 5); i++) {
    
        ofDrawBitmapString(luaLogs[i], fboCanvas->x, fboCanvas->y + fboCanvas->height+  60 + i*20);
        // ofLog(OF_LOG_NOTICE,luaLogs[i]);

    }
    
}

bool CanvasManager::hasLuaScript() {
    
    
    ofApp *app = (ofApp*)ofGetAppPtr();
    
    /*
    app->fft.clear();
    app->fft = app->eq.getSpectrum();
     */
    
    
    int i =0;
    for( auto layer : layers ) {
        
        
        if(layer->bActive && layer->type == "video") {
            VideoDrawable * video = dynamic_cast<VideoDrawable*>(layer);
            
            float volume = app->eq.smoothedVol;
            
            
            float vs = ofMap(volume, 0, 1.0, 0.2, 0.8);
            float vs2 = ofMap(vs, 0.2, 0.8, 0, 1.1);

            video->processFFT(ofClamp(vs2, 0,1));
            
            
            video = NULL;
            
        }

        
        
        if(layer->bActive && layer->type == "script") {
            
            
            LuaDrawable * luaLayer = dynamic_cast<LuaDrawable*>(layer);
            
            //log = luaLayer->lua.getString("log");
            
            luaLayer->lua.setFloat("eq0", app->eq.leftPreview[0]);
            luaLayer->lua.setFloat("eq1", app->eq.leftPreview[64]);
            luaLayer->lua.setFloat("eq2", app->eq.leftPreview[128]);
            luaLayer->lua.setFloat("eq3", app->eq.leftPreview[192]);
            luaLayer->lua.setFloat("eq4", app->eq.leftPreview[256]);
            luaLayer->lua.setFloat("eq5", app->eq.leftPreview[320]);
            luaLayer->lua.setFloat("eq6", app->eq.leftPreview[384]);
            luaLayer->lua.setFloat("eq7", app->eq.leftPreview[448]);
            
            luaLayer->lua.setFloat("smoothVol", app->eq.smoothedVol);

            
            luaLayer->lua.setFloatVector("fft", app->eq.fftVec);
            
            
            
            luaLayer = NULL;
            
            
        }
        
        
        
    }
    
    app = NULL;
    return false;
    
}

void CanvasManager::startGifTest() {
    
    /*
    currentGifTest = 0;
    
    for( auto &layer : layers ) {
        
        GifGroupDrawable * gifGroup = dynamic_cast<GifGroupDrawable*>(layer);
        
        
        ofLog(OF_LOG_NOTICE, layer->type);

        if(layer->type =="gifgroup") {
            
            ofAddListener(gifGroup->gifLoaded, this, &CanvasManager::onGifLoadedHandler);
            ofAddListener(gifGroup->allLoadedEvent, this, &CanvasManager::onAllGifLoadedHandler);
            gifs.push_back(gifGroup);

        }
        
    }
    
    gifs[currentGifTest]->currentGif = 0;
    gifs[currentGifTest]->loadNext();
    
     */
    
    
}

void CanvasManager::onGifLoadedHandler(ofEventArgs & e) {
    gifs[currentGifTest]->loadNext();

}

void CanvasManager::onAllGifLoadedHandler(ofEventArgs & e) {
    
    currentGifTest++;
    gifs[currentGifTest]->currentGif = 0;

    gifs[currentGifTest]->loadNext();

    
}



//--------------------------------------------------------------
void CanvasManager::errorReceived(string& msg) {
    

    if(luaLogs.size()<5){
        luaLogs.push_back(msg);

    } else {
        int index = logCount % 5;
        luaLogs[index] = msg;
    }
}





