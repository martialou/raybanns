//
//  CanvasManager.hpp
//  RayBanNS
//
//  Created by Martial on 19/04/16.
//
//

#ifndef CanvasManager_hpp
#define CanvasManager_hpp

#include <stdio.h>
#include "ofMain.h"
#include "CanvasDrawable.hpp"
#include "ofxXmlSettings.h"
#include "ScenariiData.hpp"
#include "TransitionData.hpp"
#include "TransitionManager.hpp"
#include "TransitionTimer.hpp"
#include "ofxLua.h"
#include "GifGroupDrawable.hpp"
#include "RBMask.hpp"


struct sortByZIndex {
    bool operator()(const shared_ptr<CanvasDrawable> * lhs, const shared_ptr<CanvasDrawable>  *rhs) const {
        return lhs->get()->zIndex < rhs->get()->zIndex;
    }
};


class CanvasManager : public ofxLuaListener {
    
public:
    
    CanvasManager();
    void setup(FBOCanvas * fboCanvas);
    void update();
    void draw();
    
    void setFromXml();
    void setFromFolders();
    
    void setOnTop();
    void setOnBack();
    
    void drawLuaLog();
    
    void loadLayer(string name);
    void loadScenarii(int index);
    void loadRandomScenarii();
    void loadRandomScenariiIntroGroup();
    void onScenariiLoadedHandler();
    void onTransitionInHandler(ofEventArgs & e);
    
    // for gif testing
    void startGifTest();
    void onGifLoadedHandler(ofEventArgs & e);
    void onAllGifLoadedHandler(ofEventArgs & e);
    int currentGifTest;
    vector<GifGroupDrawable *> gifs;

    
    void errorReceived(string& msg);

    CanvasDrawable   * getLayerByName(string name);
    
    vector<string> getLayersNames(string type);
    vector<string> getScenariisNames();

    //ofxLua                      lua;
    TransitionTimer             transitionTimer;

    //void runLuaTests();
    
    bool                        hasLuaScript();
    string                      log;
    
    RBMask                      mask;

    
    int                         soundDeviceId;
    int                         transitionTimerDelay;
    float                       equalizerLucks;

    
private:
    
    TransitionManager                       transitionManager;
    vector<CanvasDrawable* >                layers;
    vector<ScenariiGroupData>               scenariiGroups;
    vector<ScenariiData>                    scenariis;
    vector<TransitionData>                  transitions;
    vector<CanvasDrawable *>     inLayers;

    vector<string>              luaLogs;
    FBOCanvas *                 fboCanvas;
    
    int                         logCount;
    
    int                         groupIndex;
    int                         totalClaims, totalEqualizer;
    int                         equalizerIndex;
    bool                        bSwitchBrand;
    string                      loadType;
};

#endif /* CanvasManager_hpp */
