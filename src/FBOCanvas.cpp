//
//  FBOCanvas.cpp
//  RaybanNS
//
//  Created by Martial on 14/04/16.
//
//

#include "FBOCanvas.hpp"

void FBOCanvas::setup (int width, int height) {
    
    ofxDragAndZoomable();

    
    this->width             = width;
    this->height            = height;
    this->initialWidth      = width;
    this->initialHeight     = height;
    //this->bTiltShiftEnabled = bTiltShiftEnabled;
    
    fbo.allocate(width, height);
    //post.init(width, height);
    
    /*
    rgbShiftPass = post.createPass<RGBShiftPass>();
    rgbShiftPass->setEnabled(true);
    rgbShiftAmount.reset(0);
    rgbShiftAmount.setCurve(EASE_IN);
    rgbShiftAmount.setRepeatType(PLAY_ONCE);
    
    blurPass = post.createPass<ZoomBlurPass>();
    blurPass->setEnabled(true);
    blurPassAmount.reset(0);
    blurPassAmount.setCurve(EASE_IN);
    blurPassAmount.setRepeatType(PLAY_ONCE);
    
    tiltShiftPass = post.createPass<HorizontalTiltShifPass>();
    tiltShiftPass->setEnabled(bTiltShiftEnabled);
*/
    

    //post.setFlip(false);
    
    ofAddListener(ofEvents().mouseReleased, this, &FBOCanvas::onMouseReleavedEvent);
    ofAddListener(ofEvents().mousePressed, this, &FBOCanvas::onMousePressedEvent);
    ofAddListener(ofEvents().mouseDragged, this, &FBOCanvas::onMouseDragEvent);
    ofAddListener(ofEvents().keyPressed, this, &FBOCanvas::onKeyPressedEvent);
    ofAddListener(ofEvents().mouseScrolled, this, &FBOCanvas::onWheelEvent);

    origRect.set(x,y,width, height);
    
}

void FBOCanvas::update () {
    ofxDragAndZoomable::update();
    
    /*
    float dt = 1.0f / 60.0f;
    rgbShiftAmount.update(dt);
    rgbShiftPass->setAmount(rgbShiftAmount.getCurrentValue());
    
    blurPassAmount.update(dt);
    blurPass->setDensity(blurPassAmount.getCurrentValue());
     
     */
    
    //pixelatePass->set
}


void FBOCanvas::begin() {
    
    fbo.begin();
    ofClear(0);
    ofBackground(0, 0, 0);
    
   // post.begin();
    
  

}

void FBOCanvas::end() {
    
 //   post.end();
    fbo.end();
    
}

void FBOCanvas::drawPlaceHolder() {
    
    ofSetColor(255,0,0);
    ofDrawRectangle(0.0, 0.0, initialWidth * .5, initialHeight);
    
    ofSetColor(0,0,255);
    ofDrawRectangle(initialWidth * .5, 0.0, initialWidth * .5, initialHeight);
    
}

void FBOCanvas::drawBoundaries() {
    
    ofSetColor(255);
    ofDrawRectangle(x-2, y-2, width+4, height+4);

    
}


void FBOCanvas::draw() {
    
    //post.draw();
  //  fbo.draw(0,ofGetHeight(),ofGetWidth(),0-ofGetHeight());
    fbo.draw(this->x, this->y, this->width, this->height);

}


void FBOCanvas::onMousePressedEvent(ofMouseEventArgs & e) {
    
    if(ofGetKeyPressed(OF_KEY_SHIFT)) {
        lastClick.set(e.x - x, e.y - y);
    }
    
}

void FBOCanvas::onMouseDragEvent(ofMouseEventArgs & e) {
    
    if(ofGetKeyPressed(OF_KEY_SHIFT)) {
        drag(e.x, e.y);
    }
    
}

void FBOCanvas::onMouseReleavedEvent(ofMouseEventArgs & e) {

}

void FBOCanvas::onKeyPressedEvent(ofKeyEventArgs & e) {

    
}

void FBOCanvas::onWheelEvent(ofMouseEventArgs & e) {
    
    ofxDragAndZoomable::onWheelEvent(e.scrollY);
}

