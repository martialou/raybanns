//
//  FBOCanvas.hpp
//  RaybanNS
//
//  Created by Martial on 14/04/16.
//
//

#ifndef FBOCanvas_hpp
#define FBOCanvas_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxPostProcessing.h"
#include "ofxDragAndZoomable.h"
#include "ofxAnimatableFloat.h"

class FBOCanvas : public ofxDragAndZoomable {
    
public:
    
    void setup(int width, int height);
    void update();
    void draw();
    void drawBoundaries();

    void drawPlaceHolder();
    
    void begin();
    void end();
    
    
    int initialWidth, initialHeight;
    ofFbo fbo;

    /*
    RGBShiftPass::Ptr               rgbShiftPass;
    ofxAnimatableFloat              rgbShiftAmount;

    ZoomBlurPass::Ptr               blurPass;
    ofxAnimatableFloat              blurPassAmount;
    
    HorizontalTiltShifPass::Ptr     tiltShiftPass;
    
    bool                            bTiltShiftEnabled;
     */
    
private:
    
   // ofxPostProcessing           post;
    
    void onWheelEvent(ofMouseEventArgs & e);
    void onMousePressedEvent(ofMouseEventArgs & e);
    void onMouseDragEvent(ofMouseEventArgs & e);
    void onMouseReleavedEvent(ofMouseEventArgs & e);
    void onKeyPressedEvent(ofKeyEventArgs & e);


    
};

#endif /* FBOCanvas_hpp */
