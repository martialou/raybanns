//
//  GifDrawable.cpp
//  RayBanNS
//
//  Created by Martial on 22/04/16.
//
//

#include "GifDrawable.hpp"
#include "ofxImgSizeUtils.h"

void GifDrawable::setup() {
    
    currentFrame = 0;
    speed = .3;
}

void GifDrawable::update(float dt) {
    
    CanvasDrawable::update(dt);
    
    currentFrame += speed;
    if(currentFrame>= file.getNumFrames() )
       currentFrame = 0;
    
}

void GifDrawable::draw() {
    
    CanvasDrawable::draw();

    ofSetColor(255,255,255, opacity.getCurrentValue());
    
    ofSetColor(255,255,255, 255);

    ofPoint pnt = ofxImgSizeUtils::getSizeRatio(3648, 1080, file.getWidth(), file.getHeight(), false);
    ofPoint pos;
    pos.x = 3648 * .5 - pnt.x *.5;
    pos.y = 1080 * .5 - pnt.y *.5;
    file.drawFrame((int)currentFrame, pos.x ,  pos.y, pnt.x, pnt.y);
    

}


void GifDrawable::load(string path) {
    
    decoder.decode("gifs/" + path);
    file = decoder.getFile();

}
