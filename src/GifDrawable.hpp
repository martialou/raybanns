//
//  GifDrawable.hpp
//  RayBanNS
//
//  Created by Martial on 22/04/16.
//
//

#ifndef GifDrawable_hpp
#define GifDrawable_hpp

#include <stdio.h>
#include "CanvasDrawable.hpp"
#include "ofxGifDecoder.h"
#include "ofxGifFile.h"

class GifDrawable : public CanvasDrawable {
    
public:
    
    GifDrawable() {
        CanvasDrawable::CanvasDrawable();
        type = "gif";

    }
    void setup();
    void update(float dt);
    void draw();
    
    void load(string path);
    
private:
    
    string path;
    
    ofxGifDecoder decoder;
    ofxGifFile file;
    float currentFrame;
    float speed;
};

#endif /* GifDrawable_hpp */
