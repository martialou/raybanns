//
//  GifGroupDrawable.cpp
//  RayBanNS
//
//  Created by Martial on 22/04/16.
//
//

#include "GifGroupDrawable.hpp"

#include "ofxImgSizeUtils.h"

void GifGroupDrawable::setup() {
    
    currentGif = 0;
    currentFrame = 0;
    speed = .3;
    ofAddListener(decoder.onGifLoaded, this,&GifGroupDrawable::onGifLoadedHandler);
    //ofAddListener(decoder.decoderOut, this,&GifGroupDrawable::onGifLoadedHandler);

    bIsLoading = false;
    //decoders.push_back(&decoder, &decoderOut);
    
    for(int i=0; i< 6; i++) {

        ofFbo fbo;
        fbos.push_back(fbo);
        
    }
}

void GifGroupDrawable::update(float dt) {
    
    CanvasDrawable::update(dt);
    
    if(!bHasOneloaded)
        return;
    
    if(ofGetFrameNum() % 200 == 0 ) {
        loadRandom();
    }
    
    currentFrame += speed;
    if(currentFrame>= file.getNumFrames() )
        currentFrame = 0;
    
}


void GifGroupDrawable::draw() {
   
    if(!bHasOneloaded)
        return;
    
    CanvasDrawable::draw();
    ofSetColor(255,255,255, opacity.getCurrentValue());
    
    ofPoint pnt = ofxImgSizeUtils::getSizeRatio(3648, 1080, file.getWidth(), file.getHeight(), false);
    ofPoint pos;
    pos.x = 3648 * .5 - pnt.x *.5;
    pos.y = 1080 * .5 - pnt.y *.5;
        
    float ratio = pnt.y / 1080;
    
    if(ratio > 2.0) {
        
        int roundedRatio = round(ratio);
        if(roundedRatio > 6)
            roundedRatio = 6;
        
        
        pnt = ofxImgSizeUtils::getSizeRatio(3648/ (float)(roundedRatio), 1080, file.getWidth(), file.getHeight(), true);
        pos.x = (3648) * .5 - pnt.x *.5;
        pos.y = 1080 * .5 - pnt.y *.5;
        
        for(int i=0; i< roundedRatio; i++) {
            

            fbos[i].clear();
            fbos[i].allocate((int)(3648/ (float)(roundedRatio)), 1080);
            
        }
        for(int i=0;i <roundedRatio; i++) {
            fbos[i].begin();
            ofClear(0);
           // file.drawFrame((int)currentFrame,  0  ,  0, pnt.x, pnt.y);
            file.draw( 0  ,  0, pnt.x, pnt.y);

            fbos[i].end();
        }
        
        for(int i=0;i <roundedRatio; i++) {
            float posx = (3648/ (float)(roundedRatio) * i) + ((3648/ (float)(roundedRatio) * .5 ) - pnt.x * .5);
            fbos[i].draw(posx  ,  pos.y);
        }
        
    } else {
        
        //file.drawFrame((int)currentFrame, pos.x ,  pos.y, pnt.x, pnt.y);
        file.draw(pos.x ,  pos.y, pnt.x, pnt.y);

    }
    
   // ofLog(OF_LOG_NOTICE, "speed is %f  ", file.getDuration());
    
}

void GifGroupDrawable::loadRandom() {
    
    if(paths.size() == 0 )
        return;
    
    currentGif = floor(ofRandom(paths.size()));
    
    decoder.decodeThreaded(paths[currentGif]);
    bIsLoading = true;

 
}

void GifGroupDrawable::loadNext() {
    
    if(paths.size() == 0 || bIsLoading)
        return;
    
   
    ofEventArgs e;

    if(currentGif == paths.size()) {
        currentGif = 0;
        ofNotifyEvent(allLoadedEvent, e);
    }
    
   // ofLog(OF_LOG_NOTICE, paths[currentGif]);
    //ofLog(OF_LOG_NOTICE, "%d on %d", currentGif, paths.size());

    decoder.decodeThreaded(paths[currentGif]);
    currentGif++;
    bIsLoading = true;
}


void GifGroupDrawable::onGifLoadedHandler(ofxGifFile & e) {
    
    file.clear();
    
    file = e;

    currentFrame = 0;
    bHasOneloaded = true;
    
    
    bIsLoading = false;
    
    e.clear();
    
    //gifs.push_back(file);

}



void GifGroupDrawable::load(string path) {
    
    paths.push_back(path);

}

void GifGroupDrawable::startLoading() {
    
    loadRandom();
    
}



