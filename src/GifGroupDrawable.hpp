//
//  GifGroupDrawable.hpp
//  RayBanNS
//
//  Created by Martial on 22/04/16.
//
//

#ifndef GifGroupDrawable_hpp
#define GifGroupDrawable_hpp


#include <stdio.h>
#include "CanvasDrawable.hpp"
#include "ofxGifDecoder.h"
#include "ofxGifFile.h"

class GifGroupDrawable : public CanvasDrawable {
    
public:
    
    GifGroupDrawable() {
        
        CanvasDrawable::CanvasDrawable();
        type = "gifgroup";
        loadingIndex = 0;
        bHasOneloaded = false;
        //decoder = NULL;
        
        currentGif = 0;
        currentFrame = 0;
        speed = .3;

        
    }
    
    void setup();
    void update(float dt);
    void draw();
    
    void onGifLoadedHandler(ofxGifFile & e);

    void loadRandom();
    void loadNext();

    void load(string path);
    void startLoading();
    
    ofEvent<ofEventArgs> allLoadedEvent;
    ofEvent<ofEventArgs> gifLoaded;
    int             currentGif;
    
    
private:
    
    string          path;
    int             loadingIndex;
    vector<string>  paths;
    ofxGifFile      file;
    ofxGifDecoder   decoder;

    float           currentFrame;
    float           speed;
    bool            bHasOneloaded;
    bool            bIsLoading;
    vector<ofxGifFile> gifs;
    vector<ofFbo>   fbos;
    
};

#endif /* GifDrawable_hpp */
