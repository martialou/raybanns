//
//  GuiManager.cpp
//  RayBanNS
//
//  Created by Martial on 14/04/16.
//
//

#include "GuiManager.hpp"
#include "ofApp.h"

void GuiManager::setup() {
    
    combo2DropDown = NULL;
    combo3DropDown = NULL;
    
    blockWidth      = 200;
    bDrawPlaceHolder = false;
    bPublishSyphon = true;
    bSkipEqualizer = false;
    
    gui = new ofxDatGui( ofxDatGuiAnchor::TOP_LEFT );
    gui->setWidth(blockWidth);
    gui->setAssetPath(ofToDataPath(""));
    gui->addFRM();
    gui->addBreak();
    
    ofxDatGuiFolder* folder = gui->addFolder("SETTINGS", ofColor::white);
    folder->addToggle("SYPHON", true);
    folder->addToggle("PLACEHOLDER");
    folder->addToggle("FIT TO SCREEN");
    folder->addToggle("SKIP SOUND REACTIVE", true);

    folder->addToggle("AUTO MODE", true);

    folder->setWidth(blockWidth);
    
    folder->onButtonEvent(this, &GuiManager::onButtonEvent);
    folder->onSliderEvent(this, &GuiManager::onSliderEvent);
    folder->onTextInputEvent(this, &GuiManager::onTextInputEvent);
    folder->on2dPadEvent(this, &GuiManager::on2dPadEvent);
    folder->onColorPickerEvent(this, &GuiManager::onColorPickerEvent);
    
    /*
    folder = gui->addFolder("PostProcessing", ofColor::white);
    folder->addToggle("TSHIFT");

    folder->setWidth(blockWidth);
    
    folder->onButtonEvent(this, &GuiManager::onButtonEvent);
    folder->onSliderEvent(this, &GuiManager::onSliderEvent);
    folder->onTextInputEvent(this, &GuiManager::onTextInputEvent);
    folder->on2dPadEvent(this, &GuiManager::on2dPadEvent);
    folder->onColorPickerEvent(this, &GuiManager::onColorPickerEvent);
     */

    
    load();
}

void GuiManager::draw() {

    menu->update();
    menu->draw();
    
    scriptsMenu->update();
    scriptsMenu->draw();
    
    gifsMenu->update();
    gifsMenu->draw();
    
    comboDropDown->update();
    comboDropDown->draw();
    
    if(combo2DropDown) {
    combo2DropDown->update();
    combo2DropDown->draw();
    }
    
    if(combo3DropDown) {
        combo3DropDown->update();
        combo3DropDown->draw();
    }
    
    
    
}

void GuiManager::save() {
}

void GuiManager::load() {
}

void GuiManager::addLayersDropDown(vector<string> layers) {
    
    menu = new ofxDatGuiDropdown("VIDEOS", layers);
    menu->setPosition(blockWidth, 0);
    menu->setWidth(200);
    menu->onDropdownEvent(this, &GuiManager::onDropdownEvent);
   // menu->expand();
}

void GuiManager::addScriptsDropDown(vector<string> layers) {
    
    scriptsMenu = new ofxDatGuiDropdown("SCRIPTS", layers);
    scriptsMenu->setPosition(blockWidth*3, 0);
    scriptsMenu->setWidth(200);
    scriptsMenu->onDropdownEvent(this, &GuiManager::onDropdownEvent);
   // scriptsMenu->expand();
    
}

void GuiManager::addCombosDropDown(vector<string> layers) {
    
    comboDropDown = new ofxDatGuiDropdown("COMBOS", layers);
    comboDropDown->setPosition(blockWidth*4, 0);
    comboDropDown->setWidth(200);
    comboDropDown->onDropdownEvent(this, &GuiManager::onComboDropDownEvent);
    //comboDropDown->expand();
    
}

void GuiManager::addCombos2DropDown(vector<string> layer) {
    
    combo2DropDown = new ofxDatGuiDropdown("COMBOS", layer);
    combo2DropDown->setPosition(blockWidth*5, 0);
    combo2DropDown->setWidth(200);
    combo2DropDown->onDropdownEvent(this, &GuiManager::onComboDropDownEvent);
    
}

void GuiManager::addCombos3DropDown(vector<string> layer) {
    
    combo3DropDown = new ofxDatGuiDropdown("COMBOS", layer);
    combo3DropDown->setPosition(blockWidth*6, 0);
    combo3DropDown->setWidth(200);
    combo3DropDown->onDropdownEvent(this, &GuiManager::onComboDropDownEvent);
    
}



void GuiManager::addGifsDropDown(vector<string> layers) {
    
    gifsMenu = new ofxDatGuiDropdown("GIFS", layers);
    gifsMenu->setPosition(blockWidth*2, 0);
    gifsMenu->setWidth(200);

    gifsMenu->onDropdownEvent(this, &GuiManager::onDropdownEvent);
    //gifsMenu->expand();
    
    
}

void GuiManager::onComboDropDownEvent(ofxDatGuiDropdownEvent e) {
    ofApp *app = (ofApp*)ofGetAppPtr();
    cout << "dropdown: " << e.target->getLabel()  << endl;
    app->manager.loadScenarii(ofToInt(e.target->getLabel()));
}


void GuiManager::onDropdownEvent(ofxDatGuiDropdownEvent e) {
    
    ofApp *app = (ofApp*)ofGetAppPtr();
    cout << "dropdown: " << e.target->getLabel()  << endl;
    app->manager.loadLayer(e.target->getLabel());
    
}

void GuiManager::onSliderEvent(ofxDatGuiSliderEvent e)
{
    //GuiManager << "onSliderEvent: " << e.target->getLabel() << " "; e.target->printValue();
    //if (e.target->is("datgui opacity")) gui->setOpacity(e.scale);
}

void GuiManager::onButtonEvent(ofxDatGuiButtonEvent e)
{
    ofApp *app = (ofApp*)ofGetAppPtr();

    if (e.target->is("SYPHON")) {
        bPublishSyphon = e.target->getEnabled();
    }
    if (e.target->is("PLACEHOLDER")) {
        bDrawPlaceHolder = e.target->getEnabled();
    }
    
    if (e.target->is("FIT TO SCREEN")) {
        app->FBOCanvas.fitToScreen();        
    }
    
    if (e.target->is("SKIP SOUND REACTIVE")) {
        bSkipEqualizer = !bSkipEqualizer;
        
        app->manager.equalizerLucks = (bSkipEqualizer) ? 0.0 : 0.45;
        
    }
    
    if (e.target->is("AUTO MODE")) {
        app->manager.transitionTimer.bEnabled = e.target->getEnabled();
    }
    

    
    
    
    //cout << "onButtonEvent: " << e.target->getLabel() << " " << e.target->getEnabled() << endl;
}

void GuiManager::onTextInputEvent(ofxDatGuiTextInputEvent e)
{
    cout << "onTextInputEvent: " << e.target->getLabel() << " " << e.target->getText() << endl;
}

void GuiManager::on2dPadEvent(ofxDatGui2dPadEvent e)
{
    cout << "on2dPadEvent: " << e.target->getLabel() << " " << e.x << ":" << e.y << endl;
}



void GuiManager::onColorPickerEvent(ofxDatGuiColorPickerEvent e)
{
    cout << "onColorPickerEvent: " << e.target->getLabel() << " " << e.target->getColor() << endl;
    ofSetBackgroundColor(e.color);
}

void GuiManager::onMatrixEvent(ofxDatGuiMatrixEvent e)
{
    cout << "onMatrixEvent " << e.child << " : " << e.enabled << endl;
    cout << "onMatrixEvent " << e.target->getLabel() << " : " << e.target->getSelected().size() << endl;
}

void GuiManager::fitToScreenBtnHandler() {
    
    ofApp *app = (ofApp*)ofGetAppPtr();
    app->FBOCanvas.fitToScreen();
    
}
