//
//  GuiManager.hpp
//  RayBanNS
//
//  Created by Martial on 14/04/16.
//
//

#ifndef GuiManager_hpp
#define GuiManager_hpp

#include <stdio.h>
#include "ofxDatGui.h"

class GuiManager {
    
public:
    
    void setup();
    void draw();
    
    void save();
    void load();
    
    void fitToScreenBtnHandler();
    
    void addLayersDropDown(vector<string> layers);
    void addScriptsDropDown(vector<string> layers);
    void addGifsDropDown(vector<string> layers);
    void addCombosDropDown(vector<string> layer);
    void addCombos2DropDown(vector<string> layer);
    void addCombos3DropDown(vector<string> layer);

    
    bool bPublishSyphon;
    bool bDrawPlaceHolder;
    bool bSkipEqualizer;
    
private:
    
    ofxDatGui      *  gui;
    ofxDatGuiDropdown* menu;
    ofxDatGuiDropdown* scriptsMenu;
    ofxDatGuiDropdown* gifsMenu;
    ofxDatGuiDropdown * comboDropDown, *combo2DropDown, *combo3DropDown;
    int                 blockWidth;

   
    void onDropdownEvent(ofxDatGuiDropdownEvent e);
    void onComboDropDownEvent(ofxDatGuiDropdownEvent e);
    void onButtonEvent(ofxDatGuiButtonEvent e);
    void onSliderEvent(ofxDatGuiSliderEvent e);
    void onTextInputEvent(ofxDatGuiTextInputEvent e);
    void on2dPadEvent(ofxDatGui2dPadEvent e);
    void onColorPickerEvent(ofxDatGuiColorPickerEvent e);
    void onMatrixEvent(ofxDatGuiMatrixEvent e);
    
};

#endif /* GuiManager_hpp */
