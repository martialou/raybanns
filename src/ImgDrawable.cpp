//
//  ImgDrawable.cpp
//  RayBanNS
//
//  Created by Martial on 19/04/16.
//
//

#include "ImgDrawable.hpp"

void ImgDrawable::setup() {
}

void ImgDrawable::update(float dt) {
    CanvasDrawable::update(dt);
}

void ImgDrawable::draw() {
    ofSetColor(255);
    img.draw(0.0, 0.0);
}

void ImgDrawable::loadImage(string path) {
    
    img.load(path);
}