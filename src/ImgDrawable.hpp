//
//  ImgDrawable.hpp
//  RayBanNS
//
//  Created by Martial on 19/04/16.
//
//

#ifndef ImgDrawable_hpp
#define ImgDrawable_hpp

#include <stdio.h>
#include "CanvasDrawable.hpp"
#include "ofMain.h"

class ImgDrawable : public  CanvasDrawable {
    
public:
    
    void setup();
    void update(float dt);
    void draw();
    
    void loadImage(string path);
    
private:
    
    ofImage img;
    
};

#endif /* ImgDrawable_hpp */
