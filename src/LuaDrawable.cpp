//
//  LuaDrawable.cpp
//  RayBanNS
//
//  Created by Martial on 21/04/16.
//
//

#include "LuaDrawable.hpp"
#include "CanvasManager.hpp"

void LuaDrawable::setup() {
    
    /*
    lua->scriptExit();
    lua->init();
    lua->doScript(scriptPath, true);
    lua->scriptSetup();
     
     */
    
    //lua.addListener(this);

}

void LuaDrawable::setMom(CanvasManager * manager) {
    
    //    lua.addListener(manager);
    

}

void LuaDrawable::update(float dt) {
    
/*
    checkTimer += .05;
    if (checkTimer>=1.0f){
        
        checkTimer = 0.0f;
        ofFile fileScript(ofToDataPath(scriptPath, true));
        
       time_t timestamp = std::filesystem::last_write_time(fileScript);
        
    
        if (timestamp > fileTimeStamp){
            fileTimeStamp = timestamp;
            lua.scriptExit();
            lua.init();
            lua.doScript(scriptPath, true);
            lua.scriptSetup();
            ofLog(OF_LOG_NOTICE,"init");

            
        }
    }
    time++;

    */
    
    CanvasDrawable::update(dt);
    lua.scriptUpdate();

}

void LuaDrawable::init() {
    lua.scriptSetup();

}

void LuaDrawable::exit() {
    //lua.scriptExit();
    
}

void LuaDrawable::draw() {
    
    fbo.begin();
    ofClear(0);
    lua.scriptDraw();
    fbo.end();
    
    ofSetColor(255,255,255, opacity.getCurrentValue());
    fbo.draw(0,0);
}

void LuaDrawable::setLua(ofxLua * lua) {
    //this.lua = lua;
}


void LuaDrawable::loadScript(string path) {
    this->scriptPath = path;
    lua.doScript(scriptPath, true);

}
