//
//  LuaDrawable.hpp
//  RayBanNS
//
//  Created by Martial on 21/04/16.
//
//

#ifndef LuaDrawable_hpp
#define LuaDrawable_hpp

#include <stdio.h>
#include "CanvasDrawable.hpp"
#include "ofxLua.h"

class CanvasManager;

class LuaDrawable : public CanvasDrawable {
    
public:
    
    LuaDrawable() {
        CanvasDrawable::CanvasDrawable();
        lua.init(false);
        
        fbo.allocate(3648, 1080, GL_RGBA);
        type = "script";

    }
    void setup();
    void update(float dt);
    void draw();
    
    void init();
    void exit();
    void setMom(CanvasManager * manager);
    void setLua(ofxLua * lua);
    void loadScript(string path);
    
    ofxLua      lua;

    
private:
    
    ofFbo       fbo;
    string      scriptPath;
    
    float                       checkTimer;
    int                         time;
    time_t                      fileTimeStamp;
    
    
};
#endif /* LuaDrawable_hpp */
