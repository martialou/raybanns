//
//  RBMask.cpp
//  RayBanNS
//
//  Created by Martial on 01/05/16.
//
//

#include "RBMask.hpp"

void RBMask::setup() {
    
    imageMask.load("images/clubround-mask_02_white.png");
    
    mask.allocate(3648, 1080, ofxMask::LUMINANCE);
    
  //  svg.load("images/clubround-mask.svg");
    
    
    zoom.setCurve(EASE_IN);
    zoom.setRepeatType(LOOP);
    zoom.animateFromTo(0.0, 15.0);
    zoom.setDuration(1.5);
    
    bSoundScaleReactive = false;
    bZoomEnabled = true;
    
}

void RBMask::begin() {
    mask.begin();

}

void RBMask::end() {
    mask.end();

}

void RBMask::toggleState() {
    
    
    int rdm = floor(ofRandom(3));
    
    if(rdm ==0 ) {
        toggleSoundScaleReactive();
    } else if (rdm ==1) {
        toggleZoom();
    } else {
        bZoomEnabled = false;
        bSoundScaleReactive= false;
    }
    
    
}

void RBMask::setVolume(float vol) {
    this->volume = vol;
}

void RBMask::toggleSoundScaleReactive() {
    bSoundScaleReactive = !bSoundScaleReactive;
    if(bSoundScaleReactive) {
        bZoomEnabled = false;
    }

}
void RBMask::toggleZoom() {
    bZoomEnabled = !bZoomEnabled;
    if(bZoomEnabled) {
        bSoundScaleReactive = false;
    }
    
    int rdm = floor(ofRandom(2));
    if(rdm == 0 ) {
        zoom.setRepeatType(LOOP);
    } else {
        zoom.setRepeatType(LOOP_BACK_AND_FORTH);

    }
}

void RBMask::update() {
    
    
    zoom.update(1 / 60.0f);
    
    float scaledVolume = .5 + volume * 2.0;
    mask.beginMask();
    ofEnableAlphaBlending();
    ofSetColor(255,255,255);
    ofPushMatrix();
    ofTranslate(3648 * .5, 1080 * .5);
    if(bSoundScaleReactive) {
        ofScale(scaledVolume,scaledVolume);
    }
    if(bZoomEnabled) {
        ofScale(zoom.getCurrentValue(),zoom.getCurrentValue());

    }
    ofTranslate(-3648 * .5, -1080 * .5);
    imageMask.draw(0, 0);
    ofPopMatrix();
    mask.endMask();

    
}

void RBMask::draw() {
    

    mask.draw();

   

    
}