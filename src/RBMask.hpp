//
//  RBMask.hpp
//  RayBanNS
//
//  Created by Martial on 01/05/16.
//
//

#ifndef RBMask_hpp
#define RBMask_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxMask.h"
#include "ofxAnimatableFloat.h"

class RBMask {
    
public:
    
    void setup();
    void update();
    void draw();
    void begin();
    void end();
    
    void setVolume(float vol);
    
    void toggleSoundScaleReactive();
    void toggleZoom();
    
    void toggleState();
    
private:
    
    float   volume;
    
    ofxMask mask;
    ofImage imageMask;
    
    bool    bSoundScaleReactive;
    bool    bZoomEnabled;
    ofxAnimatableFloat   zoom;
    
};

#endif /* RBMask_hpp */
