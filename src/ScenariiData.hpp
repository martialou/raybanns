//
//  ScenarriData.hpp
//  RayBanNS
//
//  Created by Martial on 20/04/16.
//
//

#ifndef ScenarriData_hpp
#define ScenarriData_hpp

#include <stdio.h>
#include "ofMain.h"

class Scenarii {
    
public:
    
    Scenarii() {
        bUseMask = false;
        name = "";
    }
    string name;
    bool bUseMask;
    
};

class ScenariiData {
    
public:
    
    int              id;
    vector<Scenarii> layers;
    
    
    
};

class ScenariiGroupData {
    
public:
    
    
    string      type;
    float timeMin, timeMax;
    vector<ScenariiData> scenariis;
    
    
    
};

#endif /* ScenarriData_hpp */
