//
//  TransitionData.hpp
//  RayBanNS
//
//  Created by Martial on 20/04/16.
//
//

#ifndef TransitionData_hpp
#define TransitionData_hpp

#include <stdio.h>
#include "ofMain.h"

class TransitionData {
    
public:
    
    float durationIn, durationOut;
    string easeIn, easeOut;
    
    bool    fade;
    bool    rgbShift;
    bool    blur;
    
};
#endif /* TransitionData_hpp */

