//
//  TransitionManager.cpp
//  RayBanNS
//
//  Created by Martial on 20/04/16.
//
//

#include "TransitionManager.hpp"
#include "CanvasManager.hpp"

void TransitionManager::setup(CanvasManager * canvasManager, FBOCanvas  * fboCanvas) {
    
    this->canvasManager = canvasManager;
    this->fboCanvas     = fboCanvas;
    
    timer.setRepeatType(PLAY_ONCE);
}


void TransitionManager::startTransitionOut(TransitionData * transition, CanvasDrawable  * out) {
    
    this->currentTransition = transition;
    
    
    
    
    if(transition->fade) {
        
        
        out->opacity.setDuration(transition->durationIn  / 1000.0f );
        out->opacity.animateTo(0.0f);
            
    }
    
    out = NULL;
    
}

void TransitionManager::setTimerIn() {
    
    ofAddListener(timer.animFinished, this, &TransitionManager::onTransitionInHandler);
    timer.setDuration(this->currentTransition->durationIn / 1000.0f);
    timer.animateTo(ofRandom(1000));
    
}


void TransitionManager::startTransitionIn(TransitionData * transition, CanvasDrawable  * in) {
    this->currentTransition = transition;

    in->bActive = true;
    in->setup();
    
    if(transition->fade) {
        in->opacity.reset(0.0f);
    }

}

void TransitionManager::update(float dt) {
    
    timer.update(dt);
    
}



void TransitionManager::onTransitionInHandler(ofxAnimatable::AnimationEvent & e) {
    
    //ofLog(OF_LOG_NOTICE, "transition in done");

    ofRemoveListener(timer.animFinished, this, &TransitionManager::onTransitionInHandler);
    
    

    ofEventArgs args;
    ofNotifyEvent(onTransationInEvent, args);
    //startTransitionOut();

    
}

void TransitionManager::startTransitionInBack(TransitionData * transition, CanvasDrawable  * in) {
    
    ofAddListener(timer.animFinished, this, &TransitionManager::onTransitionOutHandler);

    timer.setDuration(currentTransition->durationOut  / 1000.0f );
    timer.animateTo(ofRandom(1000));
    
    //ofLog(OF_LOG_NOTICE, "Start transition out");
    
        if(currentTransition->fade) {
            
            
            in->opacity.setDuration(currentTransition->durationOut  / 1000.0f );
            in->opacity.animateTo(255.0f);
            
        }
        
        if(currentTransition->rgbShift) {
            //fboCanvas->rgbShiftAmount.setDuration(currentTransition->durationIn  / 1000.0f );
           // fboCanvas->rgbShiftAmount.animateTo(0.0f);
            
        }
        
        if(currentTransition->blur) {
            //fboCanvas->blurPassAmount.setDuration(currentTransition->durationIn  / 1000.0f );
            //fboCanvas->blurPassAmount.animateTo(0.0f);
            
        }
    
    
    transition = NULL;
    this->currentTransition = NULL;
    in = NULL;

    
}

void TransitionManager::onTransitionOutHandler(ofxAnimatable::AnimationEvent & e) {
    //ofLog(OF_LOG_NOTICE, "end transition out");

    ofRemoveListener(timer.animFinished, this, &TransitionManager::onTransitionOutHandler);
    ofEventArgs args;
    ofNotifyEvent(onTransationOutEvent, args);

}