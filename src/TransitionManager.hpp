//
//  TransitionManager.hpp
//  RayBanNS
//
//  Created by Martial on 20/04/16.
//
//

#ifndef TransitionManager_hpp
#define TransitionManager_hpp

#include <stdio.h>
#include "ofMain.h"
#include "CanvasDrawable.hpp"
#include "FBOCanvas.hpp"
#include "TransitionData.hpp"
#include "ofxAnimatableFloat.h"
//#include "CanvasManager.hpp"
class CanvasManager;


class TransitionManager {
    
public:
    
    void setup(CanvasManager * canvasManager, FBOCanvas  * fboCanvas);
    
    void startTransitionOut(TransitionData * transition,CanvasDrawable  * out);
    void startTransitionIn(TransitionData * transition, CanvasDrawable  * in);
    void setTimerIn();
    
    
    void onTransitionInHandler(ofxAnimatable::AnimationEvent & e);
    void startTransitionInBack(TransitionData * transition, CanvasDrawable  * in);
    void onTransitionOutHandler(ofxAnimatable::AnimationEvent & e);
    
    void update(float dt);
    
    ofEvent<ofEventArgs> onTransationInEvent;
    ofEvent<ofEventArgs> onTransationOutEvent;
    
    
private:
    
    CanvasManager   * canvasManager;
    FBOCanvas       * fboCanvas;
    
    TransitionData *    currentTransition;
    vector<shared_ptr<CanvasDrawable> > *in, * out;
    ofxAnimatableFloat  timer;
};

#endif /* TransitionManager_hpp */
