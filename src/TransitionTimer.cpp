//
//  TransitionTimer.cpp
//  RayBanNS
//
//  Created by Martial on 30/04/16.
//
//

#include "TransitionTimer.hpp"
#include "CanvasManager.hpp"

void TransitionTimer::setup(CanvasManager * manager) {
    
    timer.setup(10000, true);
    ofAddListener(timer.TIMER_REACHED, this, &TransitionTimer::onTimerDoneHandler);
    timer.startTimer();
    
    
    timerInto.setup(2000, true);
    ofAddListener(timerInto.TIMER_REACHED, this, &TransitionTimer::onTimerIntoDoneHandler);
    timerInto.startTimer();
    
    
    this->manager = manager;
    
    bEnabled = true;
    
}

void TransitionTimer::resetTimers(float value, float steps) {
    
    ofLog(OF_LOG_NOTICE, "set timer %f %f", value, steps);
    //duration = value;
    timer.stopTimer();
    timer.setup(value, true);
    timer.startTimer();
    timerInto.stopTimer();
    timerInto.setup(value / steps, true);
    timerInto.startTimer();
    
}



void TransitionTimer::update() {
    
    //timer.update();
    
    //ofLog(OF_LOG_NOTICE, "time elapsed %f %f", timer.getTimeLeftInSeconds(), timerInto.getTimeLeftInSeconds());

    
}



void TransitionTimer::onTimerDoneHandler(ofEventArgs & e) {

    if(bEnabled)
    this->manager->loadRandomScenarii();
    
    //timer.setTimer(duration);
    
}

void TransitionTimer::onTimerIntoDoneHandler(ofEventArgs & e) {
    
    if(bEnabled)
        this->manager->loadRandomScenariiIntroGroup();
    
    //timerInto.setTimer(2000);
    
}

