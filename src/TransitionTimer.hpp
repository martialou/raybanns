//
//  TransitionTimer.hpp
//  RayBanNS
//
//  Created by Martial on 30/04/16.
//
//

#ifndef TransitionTimer_hpp
#define TransitionTimer_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxTimer.h"

class CanvasManager;

class TransitionTimer {
    
public:
    
    void setup(CanvasManager * manager);
    void update();
    void onTimerDoneHandler(ofEventArgs & e);
    void onTimerIntoDoneHandler(ofEventArgs & e);
    
    void resetTimers(float value, float steps);
    
    bool bEnabled;
    //float           duration;
    ofxTimer        timer, timerInto;

    
private:
    
    CanvasManager * manager;
};

#endif /* TransitionTimer_hpp */
