//
//  VideoDataManager.cpp
//  RayBanNS
//
//  Created by Martial on 20/04/16.
//
//

#include "VideoDataManager.hpp"


void VideoDataManager::setup() {
    
    if(xml.load("video_settings.xml")) {
        parseXml();
    }
    
}

void VideoDataManager::parseXml() {
    
    
    xml.pushTag("root");
    xml.pushTag("videos");
    

    int numVideos = xml.getNumTags("video");
    for(int i=0; i<numVideos; i++) {
        
        xml.pushTag("video", i);
        string path = xml.getValue("path", "" , 0);
        xml.popTag();

        
    }
    ofLog(OF_LOG_NOTICE," %d videos loaded", numVideos);
    
    
}
