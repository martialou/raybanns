//
//  VideoDataManager.hpp
//  RayBanNS
//
//  Created by Martial on 20/04/16.
//
//

#ifndef VideoDataManager_hpp
#define VideoDataManager_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxXmlSettings.h"



class VideoDataManager {
    
public:
    
    void setup();
    void parseXml();
    
    
private:
    ofxXmlSettings xml;
    
};

#endif /* VideoDataManager_hpp */
