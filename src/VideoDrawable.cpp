//
//  VideoDrawable.cpp
//  RayBanNS
//
//  Created by Martial on 19/04/16.
//
//

#include "VideoDrawable.hpp"

void VideoDrawable::setup() {
    
#ifdef USE_HAP
    //videoPlayer = new ofxHapPlayer();
#else
    videoPlayer = new ofVideoPlayer();

#endif
    
    cosSpeed = 80;
    
}

void VideoDrawable::update(float dt) {
    CanvasDrawable::update(dt);
    
    if(videoPlayer.isLoaded()){
        videoPlayer.update();

    } else {
        ofLog(OF_LOG_NOTICE, " video not loaded !!" + ofToString(path) );
        //loadVideo(path);
        videoPlayer.load("videos/"+path);
        videoPlayer.play();
    }
    

}

void VideoDrawable::draw() {
    
    CanvasDrawable::draw();
    
    if(videoPlayer.isLoaded()){

    ofSetColor(255,255,255, opacity.getCurrentValue());
    videoPlayer.draw(0.0, 0.0);
    }
    
  //  ofLog(OF_LOG_NOTICE, "draw video %f", opacity.getCurrentValue() );

    
}

void VideoDrawable::init() {
    CanvasDrawable::init();
    videoPlayer.play();
    
    cosSpeed = ofRandom(60, 120);

}

void VideoDrawable::exit() {
    //videoPlayer->clear();
   // ofLog(OF_LOG_NOTICE, "exit video %f", opacity.getCurrentValue() );
    CanvasDrawable::exit();
    videoPlayer.stop();

}

void VideoDrawable::processFFT(float volume) {
    
    float v = volume;
    v = 0.5 + cos((float)ofGetElapsedTimeMillis() / (float)cosSpeed) * 0.5;
        
    
    
    if(bSoundReactive)
        videoPlayer.setPosition(ofClamp(v, 0, 1));
    
    
//ofLog(OF_LOG_NOTICE, "reverse %f", v);

}


void VideoDrawable::loadVideo(string path) {
   
    this->path = path;
    
    /*
    #ifdef USE_HAP
    
    size_t position = path.find(".");
    string extractName = (string::npos == position)? path : path.substr(0, position);
    string extension = (string::npos == position)? path : path.substr(position, path.size());
    path = extractName + "_HAP" + extension;
       
#endif
     */
   // ofLog(OF_LOG_NOTICE, "load videos");

    ofLog(OF_LOG_NOTICE, path);

    
}