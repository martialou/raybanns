//
//  VideoDrawable.hpp
//  RayBanNS
//
//  Created by Martial on 19/04/16.
//
//

#ifndef VideoDrawable_hpp
#define VideoDrawable_hpp

#include <stdio.h>
#include "CanvasDrawable.hpp"
#include "ofxHapPlayer.h"

#define USE_HAP

class VideoDrawable : public CanvasDrawable{
    
public:
    
    VideoDrawable(bool bUseHap = false) {
        CanvasDrawable::CanvasDrawable();
        this->bUseHap = bUseHap;
        bSoundReactive = false;

        setup();
        type = "video";
        bReverse = false;

    }
    
    void setup();
    void update(float dt);
    void draw();
    
    void loadVideo(string path);
    
    void init();
    void exit();
    
    void processFFT(float volume);
    
    bool bSoundReactive, bReverse;
    float cosSpeed;
    
private:
    
    bool bUseHap;
    string path;
    
#ifdef USE_HAP
    ofxHapPlayer     videoPlayer;
#else
    ofVideoPlayer   *  videoPlayer;
#endif
};

#endif /* VideoDrawable_hpp */
