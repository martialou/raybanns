#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    
    ofSetVerticalSync(true);
    ofSetFrameRate(60);
    ofEnableAlphaBlending();
    ofSetCircleResolution(96);
    ofSetLogLevel(OF_LOG_NOTICE);
    
    //videoDataManager.setup();
    guiManager.setup();
    FBOCanvas.setup(3648, 1080);
    //ofSetCoordHandedness(OF_RIGHT_HANDED);
    
    syphon.setName("RAYBAN NS");

    manager.setup(&FBOCanvas);
    manager.setFromXml();
    
    FBOCanvas.fitToScreen();
    
    guiManager.addLayersDropDown(manager.getLayersNames("video"));
    guiManager.addScriptsDropDown(manager.getLayersNames("script"));
    guiManager.addGifsDropDown(manager.getLayersNames("gifgroup"));
    
    
    vector<string> combosNames = manager.getScenariisNames();
    vector<string> first;
    vector<string> second;
    vector<string> third;
    
    for (int i=0; i<combosNames.size(); i++) {
        
        if(i > 60) {
            third.push_back(combosNames[i]);
        } else if(i> 30 && i < 60) {
            second.push_back(combosNames[i]);
        } else {
            first.push_back(combosNames[i]);
        }
        
    }
    
    guiManager.addCombosDropDown(first);
    if(second.size()>0) {
        guiManager.addCombos2DropDown(second);

    }
    
    if(third.size()>0) {
        guiManager.addCombos3DropDown(third);
        
    }
    
    /* EQ */
    soundStream.setDeviceID(manager.soundDeviceId);
    soundStream.setup(this, 0,2, 44100, 512, 4);
   // ofSoundStreamSetup(0,2,this, 44100, 512, 4);
    eq.setup();
    eq.setRange(256);
    eq.smooth = 2.2;
   
    manager.loadRandomScenarii();

  }

//--------------------------------------------------------------
void ofApp::update(){
    
    manager.hasLuaScript();
    FBOCanvas.update();
    manager.update();
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    ofBackground(0, 0, 0);
    
    
    FBOCanvas.begin();

    if(guiManager.bDrawPlaceHolder)
        FBOCanvas.drawPlaceHolder();
    else
        manager.draw();
    
    FBOCanvas.end();
    FBOCanvas.drawBoundaries();
    FBOCanvas.draw();
    
    guiManager.draw();
    

    if(guiManager.bPublishSyphon)
        syphon.publishTexture(&FBOCanvas.fbo.getTexture());
}

//--------------------------------------------------------------
void ofApp::audioReceived (float * input, int bufferSize, int nChannels){
    
    
    eq.audioReceived(input, bufferSize);
    
    manager.mask.setVolume(eq.smoothedVol);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    //manager.loadScenarii((int)ofRandom(2));
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
//--------------------------------------------------------------
void ofApp::exit() {
    guiManager.save();
    
}
